import os
import json
import numpy as np
from warrant import Cognito

from server_scripts import services

DATA_DIR = "data/server-trips/"

def authorize():
    cognito_user = Cognito(
        os.environ['USER_POOL_ID'],
        os.environ['CLIENT_ID'],
        access_key="fake",
        secret_key="deep_fake",
        user_pool_region=os.environ['AWS_REGION'],
    )
    cognito_user.username = os.environ['USERNAME']
    cognito_user.authenticate(password=os.environ['PASSWORD'])
    return cognito_user.id_token

def get_policies(token, uid, start_date=None, end_date=None):
    policies_api = services.FDApi(token)
    policies_todo = []
    url_builder = services.FDUrl()

    limit = 20
    offset = 0
    while True:
        url = url_builder.policies_issued(uid, limit=limit, offset=offset)
        policies_raw = policies_api.get(url)
        if policies_raw == "Auth error":
            return -2
        policies = json.loads(policies_raw.content)
        for policy in policies:
            if (start_date is not None or end_date is not None) \
                and (policy['expires_at'] < start_date.timestamp() or\
                policy['expires_at'] > end_date.timestamp()):
                continue
            policies_todo.append(policy['policy_id'])

        if len(policies) < limit:
            break

        offset += limit

    result = []

    for policy in policies_todo:
        url = url_builder.trips_for_policy(uid, policy)
        trips_raw = policies_api.get(url)
        trips = json.loads(trips_raw.content)
        for trip in trips:
            result.append(trip['upload_trip_id'])

    return result

def download_parquet(token, uid, upload_id):
    documents_api = services.FDApi(token)
    url_builder = services.FDUrl()

    doc_id = upload_id.replace("-", ".")
    collection = services.PARQUET_COLLECTION_ID

    url = url_builder.document_download(uid, collection, doc_id)
    fname = DATA_DIR + uid + "/" + doc_id[0:6] + ".parquet"

    download_ok = documents_api.download_file(url, fname)

    if not download_ok:
        return -1

    return 0

def haversine_np(lat1, lon1, lat2, lon2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)

    All args must be of equal length.

    """
    lon1, lat1, lon2, lat2 = map(np.radians, [lon1, lat1, lon2, lat2])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = np.sin(dlat/2.0)**2 + np.cos(lat1) * np.cos(lat2) * np.sin(dlon/2.0)**2

    c = 2 * np.arcsin(np.sqrt(a))
    km = 6371 * c  # 6371 is Radius of earth in kilometers. Use 3956 for miles
    meters = km * 1000
    return meters

def trip_preprocessing(table):
    table.sort_values(by='timestamp')
    ts_first, ts_last = table.iloc[[0, -1]]['timestamp']

    gps = table.loc[table['geoPosition_latitude'].notnull()]
    table['dist'] = \
        haversine_np(gps.geoPosition_latitude.shift(), gps.geoPosition_longitude.shift(),
                     gps['geoPosition_latitude'], gps['geoPosition_longitude'])
    table['dt'] = gps['timestamp'] - gps.timestamp.shift()
    table['speed'] = (table['dist'] / 1000) / (table['dt'].dt.total_seconds() / 3600)

    table['acc_abs'] = np.sqrt(table['acceleration_x'] ** 2 + \
                       table['acceleration_y'] ** 2 + \
                       table['acceleration_z'] ** 2)

    if table['acc_abs'].mean() > 70:
        print("Acceleration is too big: enable workaround for iPhone app bug")
        table['acc_abs'] = np.sqrt(
            (table['accelerationGravity_x'] / 9.8) ** 2
            + (table['accelerationGravity_y'] / 9.8) ** 2
            + (table['accelerationGravity_z'] / 9.8) ** 2 )

    acc = table.loc[table['acc_abs'].notnull()][['timestamp', 'acc_abs']]
    acc = acc.resample('1s', on='timestamp').count()['acc_abs'].reset_index()
    acc = acc.loc[(acc.timestamp > ts_first) & (acc.timestamp < ts_last)]

    if acc['acc_abs'].mean() < 9:
        print("***** WARNING: sampling rate is less than 10Hz!!!")

    q = table.loc[table['acc_abs'].notnull() | table['speed'].notnull(), [
        'timestamp',
        'acc_abs',
        'acceleration_x',
        'acceleration_y',
        'acceleration_z',
        'speed']]
    q = q.fillna(method='ffill') # remove missing areas

    return q, acc, gps
