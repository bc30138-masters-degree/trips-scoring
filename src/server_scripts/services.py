import os
import logging
import requests

# disable annoying messages from requests package
logging.getLogger("requests").setLevel(logging.WARNING)

LOG = logging.getLogger(__name__)

PARQUET_COLLECTION_ID = "testingparquettrips"
POLICIES_ISSUED_F = "policies/v0/{uid}/issued"
POLICIES_TRIPS_F = "policies/v0/{uid}/trips/{pid}"
DOCUMENTS_POST_F = "documents/v0/{uid}/{collection}/{key}"

class FDUrl():
    def __init__(self):
        pass

    def policies_issued(self, uid, limit=20, offset=0):
        url = POLICIES_ISSUED_F.format(uid=uid)
        return "%s?limit=%s&offset=%s" % (url, limit, offset)

    def trips_for_policy(self, uid, pid):
        return POLICIES_TRIPS_F.format(uid=uid, pid=pid)

    def document_download(self, uid, collection, doc_id):
        return DOCUMENTS_POST_F.format(uid=uid, collection=collection, key=doc_id)


class FDApi():
    '''Implements API for FD19 services'''

    def __init__(self, token, api='https://fd19-trial.sredasolutions.com'):
        self.token = token
        self.api = api

    def get(self, url, *largs, **dargs):
        dargs['headers'] = self.get_headers()
        result = None
        try:
            result = requests.get(self.get_url(url), *largs, **dargs)
        except requests.exceptions.RequestException as e:
            LOG.error("Could not make GET request for [%s]: %s", url, e)

        return result

    def get_url(self, url):
        return '{}/{}'.format(self.api, url)

    def get_headers(self):
        return {'Authorization': self.token}

    def download_file(self, url, fname, *largs, **dargs):
        try:
            check = open(fname)
            LOG.warning("File %s alredy exists, skip", fname)
            check.close()
            return True
        except FileNotFoundError:
            pass

        rep = None
        try:
            rep = self.get(url)
        except Exception as error:
            LOG.error("Could not download file url:'%s': %s", url, error)
            return False

        if rep is None or rep.status_code != 200:
            if not rep is None:
                LOG.error(
                    "Could not download file http_err:%d", rep.status_code)

            LOG.error("Original url: %s", self.api + '/' + url)
            return False

        if not os.path.exists(os.path.dirname(fname)):
            os.makedirs(os.path.dirname(fname))
        try:
            with open(fname, 'wb') as out_file:
                out_file.write(rep.content)
        except Exception as error:

            LOG.error("Could not save file '%s': %s", fname, error)
            return False

        return True
