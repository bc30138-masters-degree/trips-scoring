import sys
import os
import copy
import pickle
from pathlib import Path

from matplotlib import pyplot as plt
from matplotlib import rc
from matplotlib.backends.backend_pdf import PdfPages
from joblib import Parallel, delayed
import pandas as pd
import numpy as np
from numpy import sin, cos
from skinematics.sensors.manual import MyOwnSensor
from scipy.spatial.transform import Rotation
from scipy.signal import butter, lfilter, freqs
from skimage.restoration import denoise_tv_chambolle

from trips_scoring.utils import plot_compare, NUM_CORES


def sma(data_set: pd.DataFrame, feature: str, window='20ms'):
    return data_set[feature].rolling(window).mean()


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low')
    return b, a


def butter_lowpass_filter(data, feature, cutoff=5, order=2):
    fs = len(data) / (data.index[-1] -
                      data.index[0]).total_seconds()
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data[feature])
    return y


def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high')
    return b, a


def butter_highpass_filter(data, feature, cutoff=5, order=2):
    fs = len(data) / (data.index[-1] -
                      data.index[0]).total_seconds()
    b, a = butter_highpass(cutoff, fs, order=order)
    y = lfilter(b, a, data[feature])
    return y


def orient(data_set, silent=False):
    print('Orienting uid\'s {} data set with trip_id {}'.format(
        data_set['uid'], data_set['trip_id']))
    oriented_data_set = copy.deepcopy(data_set)
    if silent:
        sys.stdout = open(os.devnull, 'w')
    if 'magnet' in oriented_data_set:
        imu_sensors_data = {
            "acc": oriented_data_set['acc_l'][['x', 'y', 'z']].values,
            "omega": oriented_data_set['gyro'][['x', 'y', 'z']].values,
            "mag": oriented_data_set['magnet'][['x', 'y', 'z']].values,
            "rate": 100
        }
        imu_sensors = MyOwnSensor(
            in_data=imu_sensors_data, q_type="madgwick")
    else:
        imu_sensors_data = {
            "acc": oriented_data_set['acc_l'][['x', 'y', 'z']].values,
            "omega": oriented_data_set['gyro'][['x', 'y', 'z']].values,
            "rate": 100
        }
        imu_sensors = MyOwnSensor(
            in_data=imu_sensors_data)
    if silent:
        sys.stdout = sys.__stdout__
    euler_rotation_rads = Rotation.from_quat(
        imu_sensors.quat).as_euler('xyz', degrees=False)

    def R_x(x):
        return np.array([[1,      0,       0],
                         [0, cos(-x), -sin(-x)],
                         [0, sin(-x), cos(-x)]])

    def R_y(y):
        return np.array([[cos(-y), 0, -sin(-y)],
                         [0,      1,        0],
                         [sin(-y), 0, cos(-y)]])

    def R_z(z):
        return np.array([[cos(-z), -sin(-z), 0],
                         [sin(-z), cos(-z), 0],
                         [0,      0,       1]])

    line = np.array([oriented_data_set['acc_l']['x'],
                     oriented_data_set['acc_l']['y'],
                     oriented_data_set['acc_l']['z']])

    earth_linear = np.empty(line.shape)

    for i in range(len(euler_rotation_rads)):
        earth_linear[:, i] = R_z(euler_rotation_rads[i][2]) @ R_y(
            euler_rotation_rads[i][1]) @ R_x(euler_rotation_rads[i][0]) @ line[:, i]

    oriented_data_set['acc_l']['x'] = earth_linear[0, :]
    oriented_data_set['acc_l']['y'] = earth_linear[1, :]
    oriented_data_set['acc_l']['z'] = earth_linear[2, :]

    return oriented_data_set


def preprocess_single_trip(
        data_set=None,
        skip_orient=False,
        load_orient=False,
        load_uid=None,
        load_trip_id=None,
        save_orient=False,
        folder='data/pickle',
        noise_filter="tv",
        plot_list=[],
        plot_root='data/images'):
    if not skip_orient:
        if load_orient:
            if 'oriented' in plot_list:
                origin_data_set = pickle.load(
                    open("{}/{}/origin_{}.p".format(
                        folder,
                        load_uid,
                        load_trip_id
                    ), "rb"))
            data_set = pickle.load(
                open("{}/{}/oriented_{}.p".format(
                    folder,
                    load_uid,
                    load_trip_id
                ), "rb"))
        else:
            if 'oriented' in plot_list or save_orient:
                origin_data_set = copy.deepcopy(data_set)
            data_set = orient(data_set)

        if save_orient:
            save_folder = "{}/{}".format(folder, data_set['uid'])
            Path(save_folder).mkdir(parents=True, exist_ok=True)
            pickle.dump(origin_data_set, open(
                "{}/origin_{}.p".format(
                    save_folder,
                    data_set['trip_id']
                ), "wb")
            )
            pickle.dump(data_set, open(
                "{}/oriented_{}.p".format(
                    save_folder,
                    data_set['trip_id']
                ), "wb")
            )

        if 'oriented' in plot_list:
            Path("{}/{}".format(plot_root,
                                data_set['uid'])).mkdir(parents=True, exist_ok=True)
            plot_compare(origin_data_set, data_set, 'acc_l',
                         plot_path="{}/{}/oriented_{}.pdf".format(
                             plot_root,
                             data_set['uid'],
                             data_set['trip_id']),
                         data_range=(0.1, 0.27),
                         plot_range=6,
                         names=('Исходные<br>данные', 'Ориентированные<br>данные'))

    print('Filtering uid\'s {} data set with trip_id {}'.format(
        data_set['uid'], data_set['trip_id']))

    if 'smooth' in plot_list:
        oriented_data_set = copy.deepcopy(data_set)

    window_size = 5

    data_set['acc_l']['x'] = sma(
        data_set['acc_l'], feature='x',
        window=window_size)

    data_set['acc_l']['y'] = sma(
        data_set['acc_l'], feature='y',
        window=window_size)

    data_set['acc_l']['z'] = sma(
        data_set['acc_l'], feature='z',
        window=window_size)

    data_set['acc_l'].fillna(0, inplace=True)

    if 'smooth' in plot_list:
        plot_compare(oriented_data_set, data_set, 'acc_l',
                     plot_path="{}/{}/smooth_{}.pdf".format(
                         plot_root,
                         data_set['uid'],
                         data_set['trip_id']),
                     data_range=(0.1, 0.27),
                     plot_range=6,
                     names=('Ориентированные<br>данные', 'Сглаженные<br>данные'))

    if 'filtered' in plot_list:
        smooth_data_set = copy.deepcopy(data_set)

    if noise_filter == "tv":
        weight = 0.5
        data_set['acc_l']['x'] = denoise_tv_chambolle(
            data_set['acc_l']['x'], weight)
        data_set['acc_l']['y'] = denoise_tv_chambolle(
            data_set['acc_l']['y'], weight)
        data_set['acc_l']['z'] = denoise_tv_chambolle(
            data_set['acc_l']['z'], weight)
    elif noise_filter == "lowpass":
        cutoff = 4
        order = 4
        data_set['acc_l']['x'] = butter_lowpass_filter(
            data_set['acc_l'],
            'x',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['y'] = butter_lowpass_filter(
            data_set['acc_l'],
            'y',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['z'] = butter_lowpass_filter(
            data_set['acc_l'],
            'z',
            cutoff=cutoff,
            order=order)
    elif noise_filter == "highpass":
        cutoff = 1
        order = 1
        data_set['acc_l']['x'] = butter_highpass_filter(
            data_set['acc_l'],
            'x',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['y'] = butter_highpass_filter(
            data_set['acc_l'],
            'y',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['z'] = butter_highpass_filter(
            data_set['acc_l'],
            'z',
            cutoff=cutoff,
            order=order)
    elif noise_filter == "hilo":
        cutoff = 0.5
        order = 1
        data_set['acc_l']['x'] = butter_highpass_filter(
            data_set['acc_l'],
            'x',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['y'] = butter_highpass_filter(
            data_set['acc_l'],
            'y',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['z'] = butter_highpass_filter(
            data_set['acc_l'],
            'z',
            cutoff=cutoff,
            order=order)
        cutoff = 5
        order = 4
        data_set['acc_l']['x'] = butter_lowpass_filter(
            data_set['acc_l'],
            'x',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['y'] = butter_lowpass_filter(
            data_set['acc_l'],
            'y',
            cutoff=cutoff,
            order=order)
        data_set['acc_l']['z'] = butter_lowpass_filter(
            data_set['acc_l'],
            'z',
            cutoff=cutoff,
            order=order)

    if 'filtered' in plot_list:
        filter_string = {
            'tv': "Метод<br>полной<br>вариации",
            'lowpass': "Фильтр<br>нижних<br>частот",
            'highpass': "Фильтр<br>высоких<br>частот",
            'hilo': "Фильтры<br>низких и высоких<br>частот"
        }
        plot_compare(smooth_data_set, data_set, 'acc_l',
                     plot_path="{}/{}/filtered_{}.pdf".format(
                         plot_root,
                         data_set['uid'],
                         data_set['trip_id']),
                     data_range=(0.1, 0.27),
                     plot_range=6,
                     names=('Сглаженные<br>данные', filter_string[noise_filter]))

    if 'noise' in plot_list:
        freq = np.fft.rfftfreq(data_set['acc_l']['x'].size, d=1/50)
        fft_x = np.fft.rfft(data_set['acc_l']['x'])
        fft_y = np.fft.rfft(data_set['acc_l']['y'])
        fft_z = np.fft.rfft(data_set['acc_l']['z'])
        with PdfPages("data/images/noise.pdf") as pdf:
            rc('font', **{'family': 'serif', 'size': 20})
            rc('text', usetex=True)
            rc('text.latex', unicode=True)
            rc('text.latex', preamble=r'\usepackage[utf8]{inputenc}')
            rc('text.latex', preamble=r'\usepackage[russian]{babel}')
            plt.rcParams["figure.figsize"] = (10, 6)
            fig, [ax1, ax2, ax3] = plt.subplots(
                3, 1, sharex=True
            )
            ax1.plot(freq, abs(fft_x), c='r', )
            plt.text(1.03, 0.4, 'X',
                     horizontalalignment='center',
                     fontsize=20,
                     transform=ax1.transAxes)
            ax2.plot(freq, abs(fft_y), c='b')
            plt.text(1.03, 0.4, 'Y',
                     horizontalalignment='center',
                     fontsize=20,
                     transform=ax2.transAxes)
            ax3.plot(freq, abs(fft_z), c='g')
            plt.text(1.03, 0.4, 'Z',
                     horizontalalignment='center',
                     fontsize=20,
                     transform=ax3.transAxes)
            ax2.set_ylabel('Амплитуда быстрого фурье преобразования')
            ax3.set_xlabel('Частота (Гц)')
            pdf.savefig()
            plt.close()

    return data_set


def preprocess_data_sets(data_sets: list, skip_orient=False):
    preprocessed = []
    if not skip_orient:
        data_sets = Parallel(n_jobs=NUM_CORES)(
            delayed(orient)(data_set, True) for data_set in data_sets)
    for data_set in data_sets:
        preprocessed.append(preprocess_single_trip(
            data_set=data_set,
            skip_orient=True
        ))
    return preprocessed
