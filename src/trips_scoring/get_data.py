import os
import pandas as pd
import numpy as np

from server_scripts import authorize, get_policies, \
    download_parquet
from server_scripts.services import LOG
from trips_scoring.utils import UID


def get_single_trip(trip_file, uid=UID, data_root='data/server-trips'):
    df_dict = {}
    df = pd.read_parquet(
        '{}/{}/{}'.format(data_root, uid, trip_file),
        engine='pyarrow'
    )
    df.sort_values(by='timestamp')
    df = df.set_index('timestamp')

    try:
        if (df.index[-1] - df.index[0]) / np.timedelta64(1, 'm') < 2.0:
            print("trip should be longer than 2 minutes, skip {}".format(trip_file))
            return -1
    except IndexError:
        print("empty trip, skip {}".format(trip_file))
        return -1

    df_dict['acc_l'] = df.loc[df['acceleration_x'].notnull(),
                              ['acceleration_x',
                               'acceleration_y',
                               'acceleration_z']
                              ]

    df_dict['acc_g'] = df.loc[df['accelerationGravity_x'].notnull(),
                              ['accelerationGravity_x',
                               'accelerationGravity_y',
                               'accelerationGravity_z']
                              ]
    df_dict['gyro'] = df.loc[df['gyro_x'].notnull(),
                             ['gyro_x',
                              'gyro_y',
                              'gyro_z']
                             ]
    df_dict['acc_l'].columns = ['x', 'y', 'z']
    df_dict['acc_g'].columns = ['x', 'y', 'z']
    df_dict['gyro'].columns = ['x', 'y', 'z']
    df_dict['uid'] = uid
    df_dict['trip_id'] = trip_file.replace('.parquet', '')
    if len(df_dict['acc_l']) != len(df_dict['gyro']):
        key_to_append: str
        acc_len = len(df_dict['acc_l'])
        gyro_len = len(df_dict['gyro'])
        if acc_len > gyro_len:
            diff = acc_len - gyro_len
            key_to_append = 'gyro'
        else:
            diff = gyro_len - acc_len
            key_to_append = 'acc_l'
        diff_timestamp = df.index[-1]
        diff_x = df_dict[key_to_append].iloc[-1]['x']
        diff_y = df_dict[key_to_append].iloc[-1]['y']
        diff_z = df_dict[key_to_append].iloc[-1]['z']
        diff_df_dict = {
            'timestamp': [],
            'x': [],
            'y': [],
            'z': []
        }
        for it in range(1, diff + 1):
            diff_df_dict['timestamp'].append(
                pd.offsets.Nano(it).apply(diff_timestamp)
            )
            diff_df_dict['x'].append(diff_x)
            diff_df_dict['y'].append(diff_y)
            diff_df_dict['z'].append(diff_z)
        diff_df = pd.DataFrame(diff_df_dict)
        diff_df = diff_df.set_index('timestamp')
        df_dict[key_to_append] = df_dict[key_to_append].append(diff_df)

    return df_dict


def load_server_trips(uid=UID, data_root='data/server-trips'):
    trips = []
    for file in os.listdir("{}/{}".format(data_root, uid)):
        if file.endswith(".parquet"):
            trip = get_single_trip(file, uid, data_root)
            if not isinstance(trip, int):
                trips.append(trip)
    return trips


def download_data_from_server(uid=UID):
    token = authorize()
    policies = get_policies(token, uid)
    for policy in policies:
        result = download_parquet(token, uid, policy)
        if result == -1:
            LOG.error("Download of parquet file %s failed", policy)


def load_jair_data(data_root='data/jair') -> dict:
    acc_csv_format = '{}/aceleracaoLinear_terra.csv'
    acc_g_csv_format = '{}/acelerometro_terra.csv'
    magnet_csv_format = '{}/campoMagnetico_terra.csv'
    gyro_csv_format = '{}/giroscopio_terra.csv'

    dfs = []
    subdirs = [x[0] for x in os.walk(data_root)]
    for subdir in subdirs:
        try:
            df = {}
            df['acc_l'] = pd.read_csv(
                acc_csv_format.format(subdir),
                names=[
                    "timestamp",
                    "uptimeNanos",
                    "x",
                    "y",
                    "z"
                ],
                header=0
            )

            df['acc_g'] = pd.read_csv(
                acc_g_csv_format.format(subdir),
                names=[
                    "timestamp",
                    "uptimeNanos",
                    "x",
                    "y",
                    "z"
                ],
                header=0
            )

            df['magnet'] = pd.read_csv(
                magnet_csv_format.format(subdir),
                names=[
                    "timestamp",
                    "uptimeNanos",
                    "x",
                    "y",
                    "z"
                ],
                header=0
            )

            df['gyro'] = pd.read_csv(
                gyro_csv_format.format(subdir),
                names=[
                    "timestamp",
                    "uptimeNanos",
                    "x",
                    "y",
                    "z"
                ],
                header=0
            )

            # df['acc_l']['timestamp'] =
            df['acc_l']['timestamp'] = pd.to_datetime(
                df['acc_l']['timestamp'], format='%d/%m/%Y %H:%M:%S')
            df['acc_l']['uptimeNanos'] = pd.to_timedelta(
                df['acc_l']['uptimeNanos'] - df['acc_l']['uptimeNanos'].iloc[0], 'ns')
            df['acc_l']['timestamp'] = df['acc_l']['timestamp'].iloc[0] + \
                df['acc_l']['uptimeNanos']
            df['acc_l'] = df['acc_l'].drop('uptimeNanos', axis=1)
            df['acc_l'] = df['acc_l'].set_index('timestamp')

            df['acc_g']['timestamp'] = pd.to_datetime(
                df['acc_g']['timestamp'], format='%d/%m/%Y %H:%M:%S')
            df['acc_g']['uptimeNanos'] = pd.to_timedelta(
                df['acc_g']['uptimeNanos'] - df['acc_g']['uptimeNanos'].iloc[0], 'ns')
            df['acc_g']['timestamp'] = df['acc_g']['timestamp'].iloc[0] + \
                df['acc_g']['uptimeNanos']
            df['acc_g'] = df['acc_g'].drop('uptimeNanos', axis=1)
            df['acc_g'] = df['acc_g'].set_index('timestamp')

            df['magnet']['timestamp'] = pd.to_datetime(
                df['magnet']['timestamp'], format='%d/%m/%Y %H:%M:%S')
            df['magnet']['uptimeNanos'] = pd.to_timedelta(
                df['magnet']['uptimeNanos'] - df['magnet']['uptimeNanos'].iloc[0], 'ns')
            df['magnet']['timestamp'] = df['magnet']['timestamp'].iloc[0] + \
                df['magnet']['uptimeNanos']
            df['magnet'] = df['magnet'].drop('uptimeNanos', axis=1)
            df['magnet'] = df['magnet'].set_index('timestamp')

            df['gyro']['timestamp'] = pd.to_datetime(
                df['gyro']['timestamp'], format='%d/%m/%Y %H:%M:%S')
            df['gyro']['uptimeNanos'] = pd.to_timedelta(
                df['gyro']['uptimeNanos'] - df['gyro']['uptimeNanos'].iloc[0], 'ns')
            df['gyro']['timestamp'] = df['gyro']['timestamp'].iloc[0] + \
                df['gyro']['uptimeNanos']
            df['gyro'] = df['gyro'].drop('uptimeNanos', axis=1)
            df['gyro'] = df['gyro'].set_index('timestamp')

            df['uid'] = "jair"
            df['trip_id'] = subdir.split('/')[-1]

            dfs.append(df)

        except FileNotFoundError:
            pass

    return dfs
