import multiprocessing
import os
from pathlib import Path
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px

UID = os.environ["USER_ID"]
NUM_CORES = multiprocessing.cpu_count()

all_features = [
    'low_acceleration', 'normal_acceleration', 'aggressive_acceleration',
    'low_breaking', 'normal_breaking', 'aggressive_breaking',
    'low_right_move', 'normal_right_move', 'aggressive_right_move',
    'low_left_move', 'normal_left_move', 'aggressive_left_move'
]
naming = {
    'low_acceleration': "Медленное ускорение",
    'normal_acceleration': "Нормальное ускорение",
    'aggressive_acceleration': "Агрессивное ускорение",
    'low_breaking': "Медленное торможение",
    'normal_breaking': "Нормальное торможение",
    'aggressive_breaking': "Агрессивное торможение",
    'low_left_move': "Медленное движение влево",
    'normal_left_move': "Нормальное движение влево",
    'aggressive_left_move': "Агрессивное движение влево",
    'low_right_move': "Медленное движение вправо",
    'normal_right_move': "Нормальное движение вправо",
    'aggressive_right_move': "Агрессивное движение вправо"
}


def plot_with_labels(data_set: list, feature: str, labels=None):
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=data_set.index,
        y=data_set[feature],
        line=dict(shape='linear')
    ))

    if labels is not None:
        fig.add_trace(go.Scatter(
            x=labels['event_start'].tolist(),
            y=[10] * len(labels),
            text=labels['aggressive_level'].tolist(),
            mode="text",
        ))
        shapes = []
        for _, row in labels.iterrows():
            shapes.append(
                dict(
                    type="rect",
                    xref="x",
                    yref="paper",
                    x0=row['event_start'],
                    y0=0,
                    x1=row['event_end'],
                    y1=1,
                    fillcolor="LightSalmon",
                    opacity=0.5,
                    layer="below",
                    line_width=0,
                )
            )
        fig.update_layout(
            shapes=shapes
        )

    fig.show()


def plot(
        init_data_set: dict,
        sensor: str,
        x_axis=None,
        titles=('$X$', '$Y$', '$Z$'),
        plot_path="data/images/origin.pdf",
        data_range=None,
        plot_range=None):

    if data_range is not None:
        length = len(init_data_set[sensor])
        data_set = init_data_set[sensor][int(
            data_range[0] * length):int(
            data_range[1] * length)]
    else:
        data_set = init_data_set[sensor]

    if x_axis is None:
        x = data_set.index
    else:
        x = data_set[x_axis]

    fig = make_subplots(
        rows=2, cols=2,
        specs=[[{}, {}],
               [{"colspan": 2, "l": 0.25, "r": 0.25}, None]],
        subplot_titles=titles,
        vertical_spacing=0.15,
        horizontal_spacing=0.15,)

    fig.add_trace(
        go.Scatter(
            x=x,
            y=data_set['x'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            }
        ),
        row=1,
        col=1,
    )

    fig.add_trace(
        go.Scatter(
            x=x,
            y=data_set['y'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            }
        ),
        row=1,
        col=2
    )

    fig.add_trace(
        go.Scatter(
            x=x,
            y=data_set['z'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            }
        ),
        row=2,
        col=1
    )

    if plot_range is None:
        yx_range = max(
            (
                data_set['x'].max(),
                abs(data_set['x'].min())
            )
        )

        yy_range = max(
            (
                data_set['y'].max(),
                abs(data_set['y'].min())
            )
        )

        yz_range = max(
            (
                data_set['z'].max(),
                abs(data_set['z'].min())
            )
        )
    else:
        yx_range = plot_range
        yy_range = plot_range
        yz_range = plot_range

    fig['layout']['xaxis'].update(
        tickformat='%H:%M',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticks='outside',
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['xaxis2'].update(
        tickformat='%H:%M',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['xaxis3'].update(
        tickformat='%H:%M',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis'].update(
        range=[-yx_range, yx_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis2'].update(
        range=[-yy_range, yy_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis3'].update(
        range=[-yz_range, yz_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF')

    fig['layout']['annotations'][0].update(
        font=dict(size=14, family='Courier'),
        y=1.02
    )
    fig['layout']['annotations'][1].update(
        font=dict(size=14, family='Courier'),
        y=1.02
    )
    fig['layout']['annotations'][2].update(
        font=dict(size=14, family='Courier'),
        y=0.45
    )

    fig.update_layout(
        showlegend=False,
        margin={'l': 0, 'r': 0, 't': 30, 'b': 0},
        plot_bgcolor='white'
    )

    fig.write_image(plot_path)


def plot_compare(
        init_data_set: dict,
        init_data_set_snd: dict,
        sensor: str,
        x_axis=None,
        titles=('$X$', '$Y$', '$Z$'),
        plot_path="plot.pdf",
        data_range=None,
        plot_range=None,
        names=[
            'Исходные данные',
            'Обработанные данные'
        ]):

    if data_range is not None:
        length = len(init_data_set[sensor])
        data_set = init_data_set[sensor][int(
            data_range[0] * length):int(
            data_range[1] * length)]

        length_snd = len(init_data_set_snd[sensor])
        data_set_snd = init_data_set_snd[sensor][int(
            data_range[0] * length_snd):int(
            data_range[1] * length_snd)]
    else:
        data_set = init_data_set[sensor]
        data_set_snd = init_data_set_snd[sensor]

    if x_axis is None:
        x = data_set.index
        x_snd = data_set_snd.index
    else:
        x = data_set[x_axis]
        x_snd = data_set_snd[x_axis]

    fig = make_subplots(
        rows=2, cols=2,
        specs=[[{}, {}],
               [{"colspan": 2, "l": 0.25, "r": 0.25}, None]],
        subplot_titles=titles,
        vertical_spacing=0.15,
        horizontal_spacing=0.15,)

    fig.add_trace(
        go.Scatter(
            legendgroup="orig",
            name=names[0],
            x=x,
            y=data_set['x'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            },
        ),
        row=1,
        col=1,
    )

    fig.add_trace(
        go.Scatter(
            legendgroup="orig",
            x=x,
            y=data_set['y'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            },
            showlegend=False
        ),
        row=1,
        col=2
    )

    fig.add_trace(
        go.Scatter(
            legendgroup="orig",
            x=x,
            y=data_set['z'],
            line={
                'color': 'royalblue',
                'shape': 'linear'
            },
            showlegend=False
        ),
        row=2,
        col=1
    )

    snd_opacity = 0.8

    fig.add_trace(
        go.Scatter(
            legendgroup="snd",
            opacity=snd_opacity,
            name=names[1],
            x=x_snd,
            y=data_set_snd['x'],
            line={
                'color': 'red',
                'shape': 'linear'
            }
        ),
        row=1,
        col=1,
    )

    fig.add_trace(
        go.Scatter(
            legendgroup="snd",
            opacity=snd_opacity,
            x=x_snd,
            y=data_set_snd['y'],
            line={
                'color': 'red',
                'shape': 'linear'
            },
            showlegend=False
        ),
        row=1,
        col=2
    )

    fig.add_trace(
        go.Scatter(
            legendgroup="snd",
            opacity=snd_opacity,
            x=x_snd,
            y=data_set_snd['z'],
            line={
                'color': 'red',
                'shape': 'linear'
            },
            showlegend=False
        ),
        row=2,
        col=1
    )

    if plot_range is None:
        yx_range = max(
            (
                data_set['x'].max(),
                abs(data_set['x'].min())
            )
        )

        yy_range = max(
            (
                data_set['y'].max(),
                abs(data_set['y'].min())
            )
        )

        yz_range = max(
            (
                data_set['z'].max(),
                abs(data_set['z'].min())
            )
        )
    else:
        yx_range = plot_range
        yy_range = plot_range
        yz_range = plot_range

    fig['layout']['xaxis'].update(
        tickformat='%H:%M',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticks='outside',
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['xaxis2'].update(
        tickformat='%H:%M',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['xaxis3'].update(
        tickformat='%H:%M',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis'].update(
        range=[-yx_range, yx_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis2'].update(
        range=[-yy_range, yy_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis3'].update(
        range=[-yz_range, yz_range],
        title='$\\text{Ускорение (м/}c^2\\text{)}$',
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=14),
        title_font=dict(size=14, family='Courier'),
        tickcolor='#FFFFFF')

    fig['layout']['annotations'][0].update(
        font=dict(size=14, family='Courier'),
        y=1.02
    )
    fig['layout']['annotations'][1].update(
        font=dict(size=14, family='Courier'),
        y=1.02
    )
    fig['layout']['annotations'][2].update(
        font=dict(size=14, family='Courier'),
        y=0.45
    )

    fig.update_layout(
        margin={'l': 0, 'r': 0, 't': 30, 'b': 0},
        plot_bgcolor='white',
        legend=dict(
            x=0.77,
            y=0,
            traceorder="normal",
            font=dict(
                size=12
            ),
            bgcolor="white",
            bordercolor="Black",
        )
    )

    fig.write_image(plot_path)


def plot_clust(
        data_set: dict,
        features: list,
        clusters,
        plot_name=None,
        plot_root="data/images/clustering",
        naming=['x', 'y']):

    if len(features) != 2:
        raise AttributeError("Features list must length of 2")

    fig = px.scatter(
        data_set,
        x=features[0],
        y=features[1],
        color=clusters
    )

    fig.update_traces(marker=dict(size=12),
                      selector=dict(mode='markers'))

    offset = 0.01
    fig['layout']['xaxis'].update(
        title={
            'text': naming[0]
        },
        range=[0.0, max(data_set[features[0]]) + offset],
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticks='outside',
        ticklen=10,
        tickfont=dict(size=20, family='Serif'),
        title_font=dict(size=20, family='Serif'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis'].update(
        title={
            'text': naming[1]
        },
        range=[0.0, max(data_set[features[1]]) + offset],
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=20, family='Serif'),
        title_font=dict(size=20, family='Serif'),
        tickcolor='#FFFFFF'
    )

    fig.update_layout(
        showlegend=True,
        margin={'l': 0, 'r': 0, 't': 30, 'b': 0},
        plot_bgcolor='white',
        font=dict(size=20, family='Serif')
    )
    fig['layout']['legend']['title']['text'] = ''

    Path(plot_root).mkdir(parents=True, exist_ok=True)
    fig.write_image("{}/{}_{}.pdf".format(plot_root, features[0], features[1]))


def plot_events(
        data_set: dict,
        features: list,
        plot_root="data/images/clustering",
        naming=['x', 'y']):

    if len(features) != 2:
        raise AttributeError("Features list must length of 2")

    fig = px.scatter(
        data_set,
        x=features[0],
        y=features[1],
    )

    fig.update_traces(marker=dict(size=12),
                      selector=dict(mode='markers'))

    offset = 0.01
    fig['layout']['xaxis'].update(
        title={
            'text': naming[0]
        },
        range=[0.0, max(data_set[features[0]]) + offset],
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticks='outside',
        ticklen=10,
        tickfont=dict(size=20, family='Serif'),
        title_font=dict(size=20, family='Serif'),
        tickcolor='#FFFFFF'
    )
    fig['layout']['yaxis'].update(
        title={
            'text': naming[1]
        },
        range=[0.0, max(data_set[features[1]]) + offset],
        ticks='outside',
        linecolor='black',
        gridcolor='grey',
        mirror=True,
        ticklen=10,
        tickfont=dict(size=20, family='Serif'),
        title_font=dict(size=20, family='Serif'),
        tickcolor='#FFFFFF'
    )

    fig.update_layout(
        showlegend=False,
        margin={'l': 0, 'r': 0, 't': 30, 'b': 0},
        plot_bgcolor='white',
    )
    fig['layout']['legend']['title']['text'] = ''

    Path(plot_root).mkdir(parents=True, exist_ok=True)
    fig.write_image("{}/{}_{}.pdf".format(plot_root, features[0], features[1]))


def plot_high_pass():
    with PdfPages("data/images/hipassfilter.pdf") as pdf:
        rc('font', **{'family': 'serif', 'size': 14})
        rc('text', usetex=True)
        rc('text.latex', unicode=True)
        rc('text.latex', preamble=r'\usepackage[utf8]{inputenc}')
        rc('text.latex', preamble=r'\usepackage[russian]{babel}')
        plt.rcParams["figure.figsize"] = (10, 4.5)
        cutoff = 1
        fs = 50
        order = 2
        b, a = butter(order, cutoff, btype='high', analog=True)
        w, h = freqs(b, a)
        plt.semilogx(w, 20 * np.log10(abs(h)))
        plt.xlabel(u'Частота [рад / с]')
        plt.ylabel(u'Амплитуда [дБ]')
        plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        plt.axvline(cutoff, color='green')
        pdf.savefig()
        plt.close()


def plot_low_pass():
    with PdfPages("data/images/lopassfilter.pdf") as pdf:
        rc('font', **{'family': 'serif', 'size': 14})
        rc('text', usetex=True)
        rc('text.latex', unicode=True)
        rc('text.latex', preamble=r'\usepackage[utf8]{inputenc}')
        rc('text.latex', preamble=r'\usepackage[russian]{babel}')
        plt.rcParams["figure.figsize"] = (10, 4.5)
        cutoff = 5
        fs = 50
        order = 4
        b, a = butter(order, cutoff, btype='low', analog=True)
        w, h = freqs(b, a)
        plt.semilogx(w, 20 * np.log10(abs(h)))
        plt.xlabel(u'Частота [рад / с]')
        plt.ylabel(u'Амплитуда [дБ]')
        plt.margins(0, 0.1)
        plt.grid(which='both', axis='both')
        plt.axvline(cutoff, color='green')
        pdf.savefig()
        plt.close()


def plot_events_features(data_set, set_name):
    for idx_1, _ in enumerate(all_features):
        for idx_2 in range(idx_1 + 1, len(all_features)):
            features = [all_features[idx_1], all_features[idx_2]]
            plot_events(
                data_set,
                features,
                plot_root='data/images/events/{}/'.format(set_name),
                naming=[naming[features[0]], naming[features[1]]]
            )
