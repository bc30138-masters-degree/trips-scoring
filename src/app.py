import pickle
import time

from pathlib import Path
from joblib import Parallel, delayed
import pandas as pd
from trips_scoring.get_data import load_jair_data, \
    download_data_from_server, load_server_trips, \
    get_single_trip
from trips_scoring.preprocessing import preprocess_data_sets,\
    preprocess_single_trip, orient
from trips_scoring.scoring import events_recognition, create_events_df,\
    normalization, kmeans_clustering
from trips_scoring.utils import plot, NUM_CORES, plot_events_features, \
    plot_high_pass, plot_low_pass


def single_test_server(uid, trip_id, load=True):
    data_set = get_single_trip(trip_id + ".parquet", uid)

    plot(data_set, 'acc_l', plot_path='data/images/vasya.pdf')
    if load:
        data_set = preprocess_single_trip(
            load_orient=True,
            load_uid=uid,
            load_trip_id=trip_id,
            plot_list=(['oriented', 'smooth', 'filtered'])
        )
    else:
        data_set = preprocess_single_trip(
            data_set,
            save_orient=True,
            plot_list=(['oriented', 'smooth', 'filtered'])
        )
    print(events_recognition(data_set))


def test_save_oriented(data_sets, dump_name="oriented_big", pickle_root="data/pickle"):
    data_sets = Parallel(n_jobs=NUM_CORES)(
        delayed(orient)(data_set, True) for data_set in data_sets)
    Path(pickle_root).mkdir(parents=True, exist_ok=True)
    pickle_path = "{}/{}.p".format(pickle_root, dump_name)
    pickle.dump(data_sets, open(
        pickle_path, "wb"))

    print("Oriented data dump saved in {}".format(pickle_path))


def test_load_oriented(
        dump_name="oriented_big",
        pickle_root="data/pickle",
        events_root="data/events"):
    data_sets = pickle.load(
        open("{}/{}.p".format(pickle_root, dump_name), "rb"))
    data_sets = preprocess_data_sets(data_sets, skip_orient=True)
    events_df = create_events_df(data_sets)
    events_pickle_path = "{}/{}.csv".format(events_root,
                                            dump_name)
    events_df.to_csv(events_pickle_path, index=False)
    print("Events df .csv saved in {}".format(events_pickle_path))


def test_load_events(
        dump_name="oriented_big",
        csv_root="data/events"):
    events_df = pd.read_csv('{}/{}.csv'.format(csv_root, dump_name))
    plot_events_features(events_df, 'orig')
    plot_events_features(normalization(events_df), 'norm')
    kmeans_clustering(events_df, n_clust=4)


def main():
    data_sets = load_server_trips(uid='2000000189')
    data_sets += load_server_trips(uid='2000000222')
    data_sets += load_jair_data()
    data_sets = preprocess_data_sets(data_sets)
    events_df = create_events_df(data_sets)
    print(events_df)


# Here testing part by calling functions above
if __name__ == "__main__":
    # main()
    # data_sets = load_server_trips(uid='2000000189')
    # data_sets = load_server_trips(uid='2000000222')
    # data_sets += load_jair_data()
    # test_single_trip(data_sets[-1])
    dumps = [
        "oriented_big",
        "oriented_jair",
        "without_bad_data",
        "without_bad_data_peakless"
    ]
    dump_choice = dumps[3]
    # single_test()
    # single_test_load('2000000189', '0a3379')
    # single_test_load('2000000222', '1c1df5')
    # test_save_oriented(data_sets, dump_name=dump_choice)
    # test_save_oriented(data_sets)
    test_load_oriented(dump_choice)
    # test_load_events(dump_choice)
    # plot_high_pass()
    # plot_low_pass()
